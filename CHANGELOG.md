# Changelog

## 0.2.2

- Removed dependency versions from README.md.

## 0.2.1

- Removed irrelevant DataFrame copy in `preparation.Aggregator`.

## 0.2.0

- Updated pandas version to 0.25.

## 0.1.0

- Release.
- Fix build stage.

## 0.1.0rc1

- Initial implementation.