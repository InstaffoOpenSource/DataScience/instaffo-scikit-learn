#!/usr/bin/env bash

poetry run python -m ipykernel install --user --name instaffo-scikit-learn --display-name "Python (Instaffo Scikit-Learn)"