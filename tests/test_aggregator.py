"""
Tests for the instaffo_sklearn.preparation module.
"""
import numpy as np
import pandas as pd

from instaffo_sklearn.preparation import Aggregator, Pivotizer


def test_aggregator():
    """
    Tests the Aggregator class.
    """
    size = 1000
    col = np.random.choice(a=["A", "B", "C", "D"], size=size, replace=True)
    val = np.random.uniform(size=size)
    df_raw = pd.DataFrame(dict(col=col, val=val))

    aggregator = Aggregator(col="col", val="val")
    df_aggregated = aggregator.fit_transform(df_raw)
    assert df_aggregated.shape == (4, 2)


def test_pivotizer():
    """
    Tests the Pivotizer class.
    """
    df_raw = pd.DataFrame(
        [
            {"row": row, "col": col, "val": 1}
            for row in ["a", "b", "c", "d"]
            for col in ["e", "f", "g", "h"]
        ]
    )
    pivotizer = Pivotizer(row="row", col="col", val="val")
    csr_pivotized = pivotizer.fit_transform(df_raw)
    assert csr_pivotized.shape == (4, 4)
