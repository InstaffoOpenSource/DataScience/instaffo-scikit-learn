# instaffo-scikit-learn

instaffo-scikit-learn is a Python package that contains transformers and estimators that are compatible with the popular machine learning package [scikit-learn](https://github.com/scikit-learn/scikit-learn).

scikit-learn is the foundation of many machine learning projects here at [Instaffo](https://instaffo.com/) and we are huge fans of the tool. As we sometimes reach the limits of what is possible out of the box, we regularly create custom classes that we have decided to make open source. Please check the [license](LICENSE) for more details.

Are you curious about how we use technology to disrupt the recruiting industry? Visit our [tech blog](https://instaffo.tech/) or take a look at our [job board](https://instaffo-jobs.personio.de/).

## Installation

### Dependencies

More information about the dependencies can be found in the [pyproject.toml](pyproject.toml) file.

### User Installation

The easiest way to install instaffo-scikit-learn is using `pip`:

```
pip install instaffo-scikit-learn
```

## Changelog

See the [changelog](CHANGELOG.md) for a history of notable changes to instaffo-scikit-learn.

## Development

We welcome new contributors to this project!

### Source Code

You can check the latest sources with this command:

```
git clone git@gitlab.com:InstaffoOpenSource/DataScience/instaffo-scikit-learn.git
```

### Dependencies

To work on this project, we recommend having the following tools installed:

- [poetry](https://github.com/sdispater/poetry), for dependency management and packaging
- [pyenv](https://github.com/pyenv/pyenv), for Python version managment.

### Testing

After installation, you can launch the test suite from root:

```
poetry run tox
```

### Linting

You can launch the linting suite from root:

```
poetry run black --check .
poetry run pylint $(git ls-files | grep -E "*.py$")
```

## Help and Support

### Communication

- Jan-Benedikt Jagusch <jan@instaffo.de>
- Nikolai Gulatz <nikolai@instaffo.de>

## Acknowledgement

Thank you to [scikit-learn](https://scikit-learn.org/stable/) for their contribution to open source software!
